﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages the fractal's lifetime
/// </summary>
public class FractalManager : MonoBehaviour
{
    Fractal fractalInstance;
    public Fractal fractalPrefab;
    void Start()
    {
        ResetFractal();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            ResetFractal();
        }
    }

    void ResetFractal()
    {
        if (fractalInstance)
        {
            Destroy(fractalInstance.gameObject);
        }
        fractalInstance = Instantiate(fractalPrefab.gameObject).GetComponent<Fractal>();
    }
}
