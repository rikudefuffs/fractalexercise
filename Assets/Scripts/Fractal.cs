﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The representation of an evolving fractal
/// </summary>
public class Fractal : MonoBehaviour
{
    static Vector3[] childDirections =
    {
        Vector3.up,
        Vector3.right,
        Vector3.left,
        Vector3.forward,
        Vector3.back
    };

    static Quaternion[] childOrientations =
    {
        Quaternion.identity,
        Quaternion.Euler(0f, 0f, -90f),
        Quaternion.Euler(0f, 0f, 90f),
        Quaternion.Euler(90f, 0f, 0f),
        Quaternion.Euler(-90f, 0f, 0f)
    };

    public Mesh[] meshes;
    public Material material;
    Material[,] materials;

    public int maxDepth = 4;
    public int depth;

    public float childScale;
    public float spawnProbability;

    public float maxRotationSpeed;
    float rotationSpeed;

    bool shouldMove = false;
    public float movementSpeed = 1;

    Vector3 originalTransformForward;

    void InitializeMaterials()
    {
        materials = new Material[maxDepth + 1, 2];
        for (int i = 0; i <= maxDepth; i++)
        {
            float t = i / (maxDepth - 1f);
            t *= t; //nicer transition
            materials[i, 0] = new Material(material);
            materials[i, 0].color = Color.Lerp(Color.white, Color.yellow, t);
            materials[i, 1] = new Material(material);
            materials[i, 1].color = Color.Lerp(Color.white, Color.cyan, t);
        }
        materials[maxDepth, 0].color = Color.magenta;
        materials[maxDepth, 1].color = Color.red;

    }

    void Start()
    {
        if (materials == null)
        {
            InitializeMaterials();
        }
        rotationSpeed = Random.Range(-maxRotationSpeed, maxRotationSpeed);
        gameObject.AddComponent<MeshFilter>().mesh = meshes[Random.Range(0, meshes.Length)];
        MeshRenderer meshRenderer = gameObject.AddComponent<MeshRenderer>();
        meshRenderer.material = materials[depth, Random.Range(0, 2)];

        if (depth >= maxDepth) { return; }

        StartCoroutine(CreateChildren());
    }

    void Initialize(Fractal parent, Vector3 growthDirection, Quaternion orientation)
    {
        meshes = parent.meshes;
        material = parent.material;
        maxDepth = parent.maxDepth;
        depth = parent.depth + 1;
        childScale = parent.childScale;
        materials = parent.materials;
        spawnProbability = parent.spawnProbability;
        maxRotationSpeed = parent.maxRotationSpeed;

        transform.parent = parent.transform;

        transform.localScale = Vector3.one * childScale;
        transform.localPosition = growthDirection * (0.5f + 0.5f * childScale);
        transform.localRotation = orientation;
        originalTransformForward = transform.forward;
    }

    IEnumerator CreateChildren()
    {
        WaitForSeconds[] randomWaitTimes = new WaitForSeconds[] { new WaitForSeconds(0.1f), new WaitForSeconds(0.2f), new WaitForSeconds(0.35f) };
        int availableRandomWaitTimes = randomWaitTimes.Length;

        for (int i = 0; i < childDirections.Length; i++)
        {
            if (Random.value < spawnProbability)
            {
                yield return randomWaitTimes[Random.Range(0, availableRandomWaitTimes)];
                new GameObject("FractalChild").AddComponent<Fractal>().Initialize(this, childDirections[i], childOrientations[i]);
            }
        }
    }

    void Update()
    {
        transform.Rotate(0f, rotationSpeed * Time.deltaTime, 0f);
        if ((depth == (maxDepth - 1)) && Input.GetKeyUp(KeyCode.UpArrow))
        {
            shouldMove = true;
        }
        if (shouldMove)
        {
            transform.Translate(originalTransformForward * (Time.deltaTime * movementSpeed)); //optimized transform multiplication ;)
        }
    }
}